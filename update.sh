#!/bin/bash
#
# downloads lists of swedish governments, regions and municipalities from
# different sources, parse the data and archive relevant information.
# the data collected is "organization name" and "official web site url".
#
# sources:
# https://myndighetsregistret.scb.se/Myndighet
# https://skr.se/skr/tjanster/kommunerochregioner/
#
#
set -ueo pipefail

# this script requires a recent version of bash (v4 at least)
hash readarray

DATE=$(date "+%Y-%m-%d")
ODIR=./sources/$DATE
test -d $ODIR || mkdir -p $ODIR
ln -svfn $DATE sources/latest

# myndigheter
readarray -t myndigheter << 'EOF'
https://myndighetsregistret.scb.se/myndighet/download?myndgrupp=Statliga%20f%C3%B6rvaltningsmyndigheter&format=False
https://myndighetsregistret.scb.se/myndighet/download?myndgrupp=Myndigheter%20under%20riksdagen&format=False
https://myndighetsregistret.scb.se/myndighet/download?myndgrupp=Statliga%20aff%C3%A4rsverk&format=False
https://myndighetsregistret.scb.se/myndighet/download?myndgrupp=AP-fonder&format=False
https://myndighetsregistret.scb.se/myndighet/download?myndgrupp=Sveriges%20domstolar%20samt%20Domstolsverket&format=False
EOF

for URL in ${myndigheter[@]}; do
	# urldecode type from url parameter "myndgrupp"
	SOURCE=$(echo -e $(echo $URL | sed -E 's/.*grupp=([^&]+?).*/\1/; s/%/\\x/g'))
	echo "# $SOURCE"
	curl -s $URL | sed -E 's/\t/;/g' | cut -d\; -f2,12 > "$ODIR/$SOURCE.txt"
done

echo '# Kommuner'
curl -s https://catalog.skl.se/store/1/resource/173 | iconv -f latin1 -t utf8 | cut -d\; -f3,6 > "$ODIR/Kommuner.txt"

echo "# Regioner"
curl -s https://catalog.skl.se/store/1/resource/169 | iconv -f latin1 -t utf8 | cut -d\; -f2,5 > "$ODIR/Regioner.txt"

# remove column headers in all files
sed -i -E '1d; s/.*/\L&/' "$ODIR"/*.txt
