#!/bin/bash
#
# requirements: bash 4, curl
#
set -ueo pipefail

SRC=sources/latest
LOG=log.csv

hash readarray || (echo "this script requires bash 4 or a more recent version" && exit 1)
hash curl || (echo "this script requires curl" && exit 1)
test -d "$SRC"
declare -A pong
rm -f "$LOG"

# more info https://curl.se/docs/sslcerts.html
# curl -s --etag-compare etag.txt --etag-save etag.txt --remote-name https://curl.se/ca/cacert.pem

echo "### last run $(date '+%Y-%m-%d %H:%m:%S')"

for FILE in $SRC/*.txt; do
	echo "### $FILE"

	while read E; do
		# skip empty lines
		if [[ -z $E ]]; then
			continue
		fi

		# reset some values
		ERR=0
		RTT=0
		PING=

		# split row into two values using ; as delimiter
		# caveat: any value could be null
		NAME=${E%;*}
		SITE=${E#*;}
		HOST=$(sed -E 's/(https*:..)?([^/]+).*/\2/g' <<< "${SITE:=localhost}")

		# display some kind of feedback if we're interactive
		if [ -t 1 ] ; then
			printf "%-33s \e[33G; %-30s; check\n" "$NAME" "$HOST"
		fi

		# rudimentary cache
		if [[ -v pong[$HOST] ]]; then
			RTT=${pong[$HOST]}
		else
			RTT=$( (ping -q -W 1 -n -i 0.3 -l 3 -t 30 -c 3 "$HOST" 2> /dev/null || echo 0) | tail -1 | cut -d\/ -f 5)
			pong[$HOST]=$RTT
		fi

		curl -w "%{stderr}$NAME;%{url};%{remote_ip};%{response_code};%{errormsg};$ERR;%{scheme};%{http_version};$RTT\n" \
			-f -s -L -I -4 \
			--retry 0 \
			--connect-timeout 2 \
			-X GET "$SITE" >/dev/null 2>>"$LOG" || ERR=$?

		STAT=$([[ $ERR -ne 0 ]] && echo "fail ($ERR)" || echo "ok")

		if [ -t 1 ] ; then
			# move cursor up one row so we can overwrite previous output
			printf "\e[1A%-33s \e[33G; %-30s; %-10s\n" "$NAME" "$HOST" "$STAT"
		fi

	done < "$FILE"
done

# insert a header row in the log file
sed -i '1i name;url;server ip;response code;error message;exit code;scheme;version;avg rtt' "$LOG"

